import React from 'react';
import styled from 'styled-components';

const HeaderStyled = styled.div`
  background-color: #ff6600;
  color: white;
  font-family: 'Asap', sans-serif;
  padding: 30px;
  h1 {
    font-size: 50px;
    margin: 15px 0;
  }
  h2 {
    font-size: 18px;
    margin: 15px 0;
  }
`;

const Header = () => {
  return (
    <HeaderStyled>
      <h1>HN Feed</h1>
      <h2>We &lt; 3 hacker news!</h2>
    </HeaderStyled>
  );
};

export default Header;
