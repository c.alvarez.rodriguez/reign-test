"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
exports.__esModule = true;
var core_1 = require("@material-ui/core");
var Delete_1 = require("@material-ui/icons/Delete");
var react_1 = require("react");
var styled_components_1 = require("styled-components");
var react_moment_1 = require("react-moment");
var NewStyled = styled_components_1["default"].div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  border-bottom: 1px solid gray;\n  .date {\n    width: 100px;\n    p {\n      margin: auto;\n    }\n  }\n  .MuiListItemText-root {\n    align-items: center;\n    display: flex;\n    flex-direction: row;\n    margin: 0;\n    &hover {\n      cursor: pointer;\n    }\n  }\n  .MuiListItemText-secondary {\n    padding: 10px;\n  }\n"], ["\n  border-bottom: 1px solid gray;\n  .date {\n    width: 100px;\n    p {\n      margin: auto;\n    }\n  }\n  .MuiListItemText-root {\n    align-items: center;\n    display: flex;\n    flex-direction: row;\n    margin: 0;\n    &hover {\n      cursor: pointer;\n    }\n  }\n  .MuiListItemText-secondary {\n    padding: 10px;\n  }\n"])));
var New = function (props) {
    var calendarStrings = {
        lastDay: '[Yesterday]',
        sameDay: 'hh:mm a',
        lastWeek: 'MMM DD'
    };
    var handleOnClick = function () {
        if (window.confirm("Are you sure to delete the story: \"" + getTitle() + "\", by: " + props.data.author)) {
            props.deleteStory(props.data);
        }
    };
    var getTitle = function () {
        return props.data.title ? props.data.title : props.data.story_title;
    };
    var getUrl = function () {
        return props.data.url ? props.data.url : props.data.story_url;
    };
    return (react_1["default"].createElement(NewStyled, null,
        react_1["default"].createElement(core_1.ListItem, { button: true, component: "a", target: "_blank", href: getUrl() },
            react_1["default"].createElement(core_1.ListItemText, { primary: getTitle(), secondary: "- " + props.data.author + " -" }),
            react_1["default"].createElement("div", { className: "date" },
                react_1["default"].createElement(core_1.ListItemText, { secondary: react_1["default"].createElement(react_moment_1["default"], { calendar: calendarStrings }, props.data.created_at) })),
            react_1["default"].createElement(core_1.ListItemSecondaryAction, { onClick: handleOnClick },
                react_1["default"].createElement(core_1.IconButton, { edge: "end", "aria-label": "delete" },
                    react_1["default"].createElement(Delete_1["default"], null))))));
};
exports["default"] = New;
var templateObject_1;
