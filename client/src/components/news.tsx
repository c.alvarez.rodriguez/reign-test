import { IconButton, ListItem, ListItemSecondaryAction, ListItemText } from '@material-ui/core';
import { INews } from '../models/news.model';
import DeleteIcon from '@material-ui/icons/Delete';
import React from 'react';
import styled from 'styled-components';
import Moment from 'react-moment';

const NewStyled = styled.div`
  border-bottom: 1px solid gray;
  .date {
    width: 100px;
    p {
      margin: auto;
    }
  }
  .MuiListItemText-root {
    align-items: center;
    display: flex;
    flex-direction: row;
    margin: 0;
    &hover {
      cursor: pointer;
    }
  }
  .MuiListItemText-secondary {
    padding: 10px;
  }
`;

interface IProps {
  data: INews;
  deleteStory: Function;
}

const New = (props: IProps) => {
  const calendarStrings = {
    lastDay: '[Yesterday]',
    sameDay: 'hh:mm a',
    lastWeek: 'MMM DD',
  };

  const handleOnClick = () => {
    if (
      window.confirm(`Are you sure to delete the story: "${getTitle()}", by: ${props.data.author}`)
    ) {
      props.deleteStory(props.data);
    }
  };

  const getTitle = () => {
    return props.data.title ? props.data.title : props.data.story_title;
  };

  const getUrl = () => {
    return props.data.url ? props.data.url : props.data.story_url;
  };

  return (
    <NewStyled>
      <ListItem button component="a" target="_blank" href={getUrl()}>
        <ListItemText primary={getTitle()} secondary={`- ${props.data.author} -`} />
        <div className="date">
          <ListItemText
            secondary={<Moment calendar={calendarStrings}>{props.data.created_at}</Moment>}
          />
        </div>
        <ListItemSecondaryAction onClick={handleOnClick}>
          <IconButton edge="end" aria-label="delete">
            <DeleteIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ListItem>
    </NewStyled>
  );
};

export default New;
