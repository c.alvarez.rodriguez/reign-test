import { CircularProgress, List } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import News from './news';
import { deleteNews, getAllNews } from '../services/news.service';
import { INews } from '../models/news.model';

const NewsListStyled = styled.div`
  padding: 10px 30px;
  .load {
    display: flex;
    justify-content: center;
  }
`;

const NewsList = () => {
  const [news, setNews] = useState([]);
  const [loading, setLoading] = useState(false);

  const searchNews = async () => {
    const response = await getAllNews();
    setNews(response);
  };

  const handleOnClick = (story: INews) => {
    const index = news.findIndex((element) => element === story.objectID);
    news.splice(index, 1);
    deleteNews(story.objectID);
    setLoading(true);
  };

  useEffect(() => {
    setLoading(true);
    if (searchNews()) {
      setLoading(false);
    }
  }, [loading]);

  return (
    <NewsListStyled>
      {loading && (
        <div className="load">
          <CircularProgress />
        </div>
      )}
      {!loading && (
        <List component="ul">
          {news.map((data) => (
            <News key={data['objectID']} data={data} deleteStory={handleOnClick} />
          ))}
        </List>
      )}
    </NewsListStyled>
  );
};

export default NewsList;
