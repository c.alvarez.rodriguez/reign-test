export interface INews {
  author: string;
  created_at: string;
  objectID: string;
  story_title: string;
  story_url: string;
  title: string;
  url: string;
}
