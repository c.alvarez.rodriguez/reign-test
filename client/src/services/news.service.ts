import axios from 'axios';

export const getAllNews = async () => {
  const options = {
    url: 'http://localhost:5000/api/getAllNews',
    method: 'GET' as 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    const { data } = await axios(options);
    return data;
  } catch (error) {
    console.error(error);
  }
};

export const deleteNews = async (id: string) => {
  const options = {
    url: `http://localhost:5000/api/delete/${id}`,
    method: 'POST' as 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  try {
    const hide = await axios(options);
    return hide;
  } catch (error) {
    console.error(error);
  }
};
