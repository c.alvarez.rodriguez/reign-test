import './App.css';
import Header from './components/header';
import NewsList from './components/news-list';
import React from 'react';

function App() {
  return (
    <div className="App">
      <Header />
      <NewsList />
    </div>
  );
}

export default App;
