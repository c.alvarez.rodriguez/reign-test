# REIGN TEST

To execute this project you need follow next steps:

1. Clone the repo: git clone https://gitlab.com/c.alvarez.rodriguez/reign-test.git
2. Enter the directory: cd reign-test
3. Init .env: cp .env.example .env.
4. Run the project: docker-compose up

## Ports

- Client: http://localhost:3000
- Server: http://localhost:5000
