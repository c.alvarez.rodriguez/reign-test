import express from 'express';
import NewsRoutes from './routes/news.routes';
import cors from 'cors';
import dotenv from 'dotenv';

dotenv.config();

const app = express();

app.set('port', process.env.PORT || 5000);

app.use(cors());
app.use(express.json());

app.use('/api', NewsRoutes);

export default app;
