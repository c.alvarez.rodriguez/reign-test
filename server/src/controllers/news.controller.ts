import { Request, Response } from 'express';
import { deleteNews, getAllNews } from '../database/database';

class NewsController {
  public async searchAllNews(req: Request, res: Response) {
    try {
      const news = await getAllNews();
      res.send(news);
    } catch (error) {
      console.error(error);
    }
  }

  public async hideNews(req: Request, res: Response) {
    try {
      res.send(deleteNews(req.params.id));
    } catch (error) {
      console.error(error);
    }
  }
}

export const newsController = new NewsController();
