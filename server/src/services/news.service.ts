import axios from 'axios';
import { INews } from '../interfaces/news.interface';
const apiURL = process.env.API_URL || 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';

export const getNews = async () => {
  try {
    const res = await axios.get(apiURL);
    return res.data.hits;
  } catch (error) {
    console.error(error);
  }
};

export const filterNews = async (hits: Promise<INews[]>) => {
  try {
    return (await hits).filter((news: INews) => news.title || news.story_title);
  } catch (error) {
    console.error(error);
  }
};
