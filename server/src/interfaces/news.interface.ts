export interface INews {
  readonly author: string;
  readonly created_at: string;
  readonly objectID: string;
  readonly story_title: string;
  readonly story_url: string;
  readonly title: string;
}
