import mongoose from 'mongoose';

const mongooseURL = process.env.MONGO_URL || 'mongodb://localhost/reign';

export async function startConnection() {
  try {
    await mongoose.connect(mongooseURL, { useNewUrlParser: true, useUnifiedTopology: true });
    console.log('Database is connected');
  } catch (error) {
    console.error(error);
  }
}
