import { INews } from '../interfaces/news.interface';
import { News } from '../models/news.model';

export const saveNews = async (news: INews[]) => {
  try {
    (await news).map(async (element: INews) => {
      const story = new News(element);
      if (!(await exist(element.objectID))) {
        console.log(element);
        story.save();
      }
    });
  } catch (error) {
    console.error(error);
  }
};

export const getAllNews = async () => {
  try {
    return News.find().sort({ created_at: -1 }).limit(20);
  } catch (error) {
    console.error(error);
  }
};

export const deleteNews = async (id: string) => {
  const test = new News();
  try {
    News.findOne({ objectID: id }).then((element) => {
      if (element) {
        element.delete();
      }
    });
    return `Element with id: ${id} was deleted`;
  } catch (error) {
    console.error(error);
  }
};

const exist = async (objectID: string) => {
  try {
    return await News.findOne().select(objectID);
  } catch (error) {
    console.error(error);
  }
};
