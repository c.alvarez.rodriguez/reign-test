import { Router } from 'express';
import { newsController } from '../controllers/news.controller';
const router = Router();

router.get('/getAllNews', newsController.searchAllNews);

router.post('/delete/:id', newsController.hideNews);

export default router;
