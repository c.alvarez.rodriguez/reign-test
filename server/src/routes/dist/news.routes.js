"use strict";
exports.__esModule = true;
var express_1 = require("express");
var news_controller_1 = require("../controllers/news.controller");
var router = express_1.Router();
router.get('/getAllNews', news_controller_1.newsController.searchAllNews);
router.post('/delete/:id', news_controller_1.newsController.hideNews);
exports["default"] = router;
