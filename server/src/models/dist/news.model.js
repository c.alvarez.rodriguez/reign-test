"use strict";
exports.__esModule = true;
exports.News = void 0;
var mongoose_1 = require("mongoose");
var mongoose_delete_1 = require("mongoose-delete");
var NewsSchema = new mongoose_1["default"].Schema({
    author: String,
    created_at: String,
    deleted_at: String,
    objectID: String,
    story_title: String,
    story_url: String,
    title: String,
    url: String
});
NewsSchema.plugin(mongoose_delete_1["default"], { overrideMethods: 'all', deletedAt: true });
exports.News = mongoose_1["default"].model('news', NewsSchema);
