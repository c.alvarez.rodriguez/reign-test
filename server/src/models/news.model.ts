import mongoose, { model } from 'mongoose';
import mongoose_delete from 'mongoose-delete';

interface INewsDocument extends mongoose_delete.SoftDeleteDocument {
  author: String;
  created_at: String;
  deleted_at: String;
  objectID: String;
  story_title: String;
  story_url: String;
  title: String;
  url: String;
}

const NewsSchema = new mongoose.Schema({
  author: String,
  created_at: String,
  deleted_at: String,
  objectID: String,
  story_title: String,
  story_url: String,
  title: String,
  url: String,
});

NewsSchema.plugin(mongoose_delete, { overrideMethods: 'all', deletedAt: true });

export const News = mongoose.model<INewsDocument, mongoose_delete.SoftDeleteModel<INewsDocument>>(
  'news',
  NewsSchema
);
