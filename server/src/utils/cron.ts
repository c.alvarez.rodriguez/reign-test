import cron from 'node-cron';
import { saveNews } from '../database/database';
import { filterNews, getNews } from '../services/news.service';

export const fetchNews = () => {
  initNews();
  try {
    cron.schedule('0 * * * *', async () => {
      initNews();
    });
  } catch (error) {
    console.error(error);
  }
};

export const initNews = async () => {
  try {
    const news = await filterNews(getNews());
    if (news) {
      saveNews(news);
    }
  } catch (error) {
    console.error(error);
  }
};
