import app from './app';
import { startConnection } from './database/connection';
const cron = require('./utils/cron');

async function main() {
  startConnection();
  await app.listen(app.get('port'));
  cron.fetchNews();
  console.log('Server on port', app.get('port'));
}

main();
