"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.initNews = exports.fetchNews = void 0;
const node_cron_1 = __importDefault(require("node-cron"));
const database_1 = require("../database/database");
const news_service_1 = require("../services/news.service");
exports.fetchNews = () => {
    exports.initNews();
    try {
        node_cron_1.default.schedule('0 * * * *', () => __awaiter(void 0, void 0, void 0, function* () {
            exports.initNews();
        }));
    }
    catch (error) {
        console.error(error);
    }
};
exports.initNews = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const news = yield news_service_1.filterNews(news_service_1.getNews());
        if (news) {
            database_1.saveNews(news);
        }
    }
    catch (error) {
        console.error(error);
    }
});
