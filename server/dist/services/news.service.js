"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.filterNews = exports.getNews = void 0;
const axios_1 = __importDefault(require("axios"));
const apiURL = process.env.API_URL || 'http://hn.algolia.com/api/v1/search_by_date?query=nodejs';
exports.getNews = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const res = yield axios_1.default.get(apiURL);
        return res.data.hits;
    }
    catch (error) {
        console.error(error);
    }
});
exports.filterNews = (hits) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return (yield hits).filter((news) => news.title || news.story_title);
    }
    catch (error) {
        console.error(error);
    }
});
