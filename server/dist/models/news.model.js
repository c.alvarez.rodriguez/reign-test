"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.News = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const mongoose_delete_1 = __importDefault(require("mongoose-delete"));
const NewsSchema = new mongoose_1.default.Schema({
    author: String,
    created_at: String,
    deleted_at: String,
    objectID: String,
    story_title: String,
    story_url: String,
    title: String,
    url: String,
});
NewsSchema.plugin(mongoose_delete_1.default, { overrideMethods: 'all', deletedAt: true });
exports.News = mongoose_1.default.model('news', NewsSchema);
