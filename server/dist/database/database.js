"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteNews = exports.getAllNews = exports.saveNews = void 0;
const news_model_1 = require("../models/news.model");
exports.saveNews = (news) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        (yield news).map((element) => __awaiter(void 0, void 0, void 0, function* () {
            const story = new news_model_1.News(element);
            if (!(yield exist(element.objectID))) {
                console.log(element);
                story.save();
            }
        }));
    }
    catch (error) {
        console.error(error);
    }
});
exports.getAllNews = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return news_model_1.News.find().sort({ created_at: -1 }).limit(20);
    }
    catch (error) {
        console.error(error);
    }
});
exports.deleteNews = (id) => __awaiter(void 0, void 0, void 0, function* () {
    const test = new news_model_1.News();
    try {
        news_model_1.News.findOne({ objectID: id }).then((element) => {
            if (element) {
                element.delete();
            }
        });
        return `Element with id: ${id} was deleted`;
    }
    catch (error) {
        console.error(error);
    }
});
const exist = (objectID) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        return yield news_model_1.News.findOne().select(objectID);
    }
    catch (error) {
        console.error(error);
    }
});
