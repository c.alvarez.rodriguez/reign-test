"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const news_routes_1 = __importDefault(require("./routes/news.routes"));
const cors_1 = __importDefault(require("cors"));
const app = express_1.default();
app.set('port', process.env.PORT || 5000);
app.use(cors_1.default());
app.use(express_1.default.json());
app.use('/api', news_routes_1.default);
exports.default = app;
