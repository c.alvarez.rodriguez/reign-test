"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const news_controller_1 = require("../controllers/news.controller");
const router = express_1.Router();
router.get('/getAllNews', news_controller_1.newsController.searchAllNews);
router.post('/delete/:id', news_controller_1.newsController.hideNews);
exports.default = router;
